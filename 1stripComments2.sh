#!/bin/bash
## requires perl, latexpand and sponge
## apt install latexpand moreuits 
## maybe texlive-extra-utils
perl latexpand --empty-comments elsarticle-template.tex > manuscript_sc.tex
sed -i '/^\s*%/d' manuscript_sc.tex
cat -s manuscript_sc.tex | sponge manuscript_sc.tex